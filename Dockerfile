#  FROM debian:10-slim
#  FROM debian:stable-slim
#  FROM cmch/h520s:base
#  FROM cmch/h520s:xserver
   FROM cmch/h520s:radeon


ARG DEBIAN_FRONTEND=noninteractive


RUN \
    apt update && apt install -yq  \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    && \
    curl -fsSL https://download.docker.com/linux/debian/gpg -o /tmp/gpg && \
    apt-key add /tmp/gpg && \
    add-apt-repository \
    "deb https://download.docker.com/linux/debian buster stable" && \
    apt update && apt install -yq \
    docker-ce \
    docker-ce-cli \
    containerd.io \
    && apt clean
